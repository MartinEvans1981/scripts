import argparse
import logging
from struct import *

########################################################

def write_new_file(file_content,
                   file_path):

    logging.info('Writing new file...')

    f = open(file_path, 'w+b')
    try:
        f.write(file_content)
    except:
        logging.warning('An error occurred!')
        exit(1)
    finally:
        f.close()

    logging.info('Done!')            
 

########################################################

def append_header(file_content,
                  file_address,
                  file_xor):

    logging.info('Appending header...')

    header = pack('<lll',
                 file_address,
                 len(file_content),
                 file_xor)

    new_file_content = header + file_content

    logging.info('Done!')            

    return new_file_content

########################################################

def calc_xor(file_content):

    logging.info('Calculation xor...')

    xor_value = 0x00

    for byte in file_content:
        print hex(int(ord(byte)))
        xor_value = int(ord(byte)) ^ xor_value

    logging.info('Done, xor value is: %d', xor_value)

    return xor_value


########################################################

def file_open(file_path):

    logging.info('Reading file...')

    file_content = ""

    f = open(file_path, "rb")
    try:
        file_content = f.read()
    except:
        logging.warning('An error occurred!')
        exit(1)
    finally:
        f.close()

    logging.info('Done!')    

    return file_content

########################################################

def parse_args():

    logging.basicConfig(level=logging.INFO)
    logging.info('Parsing args...')

    parser = argparse.ArgumentParser()
    parser.add_argument("--source", 
                        help="source file path", 
                        dest="source",
                        required=True)
    parser.add_argument("--dest", 
                        help="dest file path", 
                        dest="dest",
                        required=True)

    args = parser.parse_args()

    logging.info('Done!')


    return args

########################################################

def main():

    args = parse_args()
    file_content = file_open(args.source)
    xor_value = calc_xor(file_content)

    new_file_content = append_header(file_content,
                                     0,
                                     xor_value)

    write_new_file(new_file_content,
                   args.dest)

########################################################

if __name__ == "__main__":
    main()
